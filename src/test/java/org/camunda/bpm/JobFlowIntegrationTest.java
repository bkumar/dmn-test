package org.camunda.bpm;

import com.camunda.bpm.Application;
import org.camunda.bpm.engine.rest.dto.repository.ProcessDefinitionDto;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.historyService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest(randomPort = true)
@DirtiesContext
@ActiveProfiles("test")
public class JobFlowIntegrationTest {

    @Value("${local.server.port}")
    private int port;

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void restApiIsAvailable() throws Exception {
        ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:" + port + "/rest/engine/", String.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals("[{\"name\":\"default\"}]", entity.getBody());
    }

    @Test
    public void restApiProcessDefinitions() throws Exception {
        ResponseEntity<ProcessDefinitionDto[]> entity = restTemplate.getForEntity("http://localhost:" + port + "/rest/engine/default/process-definition",
                ProcessDefinitionDto[].class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(entity.getBody().length > 0);

        boolean jobDefFound = false;
        for (ProcessDefinitionDto dto : entity.getBody()) {
            if (dto.getKey().equals("job-process")) {
                jobDefFound = true;
                break;
            }
        }
        assertTrue(jobDefFound);
    }

    @Test
    public void testProcess() {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("input1", "-50");
        variables.put("input2", "-25");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("score", 1000);
        params.put("endResult", "PASS");

        variables.put("params", params);

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("job-process", variables);
        Integer result = (Integer) historyService().createHistoricVariableInstanceQuery().processInstanceId(processInstance.getId()).variableName("score").singleResult().getValue();
        assertEquals(Integer.valueOf(925), result);
    }
}
