package org.camunda.bpm;

import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.test.DmnEngineRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class JobFlowUnitTest {

    @Rule
    public DmnEngineRule dmnEngineRule = new DmnEngineRule();

    public DmnEngine dmnEngine;
    public DmnDecision scoreDecision;

    @Before
    public void parseDecision() {
        InputStream inputStream = JobFlowUnitTest.class.getResourceAsStream("/flows/job-score.dmn");
        dmnEngine = dmnEngineRule.getDmnEngine();
        scoreDecision = dmnEngine.parseDecision("job-score", inputStream);
    }

    @Test
    public void testResult() {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("input1", "-50");
        variables.put("input2", "-25");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("score", 1000);
        params.put("endResult", "PASS");

        variables.put("params", params);

        DmnDecisionTableResult result = dmnEngine.evaluateDecisionTable(scoreDecision, variables);
        assertEquals(Integer.valueOf(925), Integer.valueOf(result.getSingleResult().getSingleEntry().toString()));
    }
}
